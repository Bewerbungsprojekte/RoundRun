package com.rocketstudio.juliusherold.roundrun.Control;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rocketstudio.juliusherold.roundrun.Helper.DBHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.ObserverHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.VariableStorage;
import com.rocketstudio.juliusherold.roundrun.Observer.DataObserver;
import com.rocketstudio.juliusherold.roundrun.R;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by juliusherold on 05.04.17.
 */

public class GameDataControl extends LinearLayout implements Observer {

    Context context;
    Cursor cursor;
    TextView currentStageTV, coinTV, maxStageTV;


    public GameDataControl(Context context, AttributeSet attr) {
        super(context, attr);
        this.context = context;
        initControl();
    }

    private void initControl() {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.gamedata_control, this);

        cursor = DBHelper.getInstance(context).getData(1);
        cursor.moveToFirst();

        int coins;
        int maxstage;


        findViews();
        try {
             coins = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_COINS));
             maxstage = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_MAXSTAGE));
        }catch (Exception e){

            coins = 0;
            maxstage = 0;

        }
        coinTV.setText(coins+"");
        maxStageTV.setText(maxstage+"");
        ObserverHelper.getInstance().getObserver().addObserver(this);




    }

    private void findViews() {
        currentStageTV = (TextView) findViewById(R.id.currentStageTV);
        coinTV = (TextView) findViewById(R.id.coinTV);
        maxStageTV = (TextView) findViewById(R.id.maxStageTV);

    }

    public void setContent(){


        currentStageTV.setText(ObserverHelper.getInstance().getObserver().getCurrentStage()+"");
        maxStageTV.setText(ObserverHelper.getInstance().getObserver().getMaxStage()+"");
        coinTV.setText(ObserverHelper.getInstance().getObserver().getCoins()+"");


    }




    @Override
    public void update(Observable o, Object arg) {
        setContent();
    }
}
