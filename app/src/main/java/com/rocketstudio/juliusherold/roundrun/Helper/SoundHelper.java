package com.rocketstudio.juliusherold.roundrun.Helper;


import android.content.Context;
import android.media.MediaPlayer;

import com.rocketstudio.juliusherold.roundrun.R;

/**
 * Created by juliusherold on 15.04.17.
 */

public class SoundHelper {

    private static SoundHelper _instance;
    Context context;
    MediaPlayer mediaPlayer;

    public static SoundHelper getInstance(Context context){

        if (_instance == null){
            _instance = new SoundHelper(context);
        }
        return _instance;

    }

    public SoundHelper(Context context){
        this.context = context;

    }

    public void playSound(){
        mediaPlayer = MediaPlayer.create(context,R.raw.sound);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

    }

    public void stopSound(){
        mediaPlayer.stop();

    }



}
