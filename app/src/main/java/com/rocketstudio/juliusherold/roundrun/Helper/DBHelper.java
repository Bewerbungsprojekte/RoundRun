package com.rocketstudio.juliusherold.roundrun.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by juliusherold on 08.01.17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SQLiteGameInfo.db";
    private static final int DATABASE_VERSION = 1;
    public static final String GAME_TABLE_NAME = "game";
    public static final String GAME_COLUMN_ID = "id";
    public static final String GAME_COLUMN_MAXSTAGE = "stage";
    public static final String GAME_COLUMN_COINS = "coins";
    public static final String GAME_COLUMN_SOUND = "sound";

    private static DBHelper _instance;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public static DBHelper getInstance(Context context){

        if (_instance == null){
            _instance = new DBHelper(context);
        }
        return _instance;

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + GAME_TABLE_NAME + "(" + GAME_COLUMN_ID + " INTEGER PRIMARY KEY, "
                + GAME_COLUMN_MAXSTAGE + " INTEGER, "
                + GAME_COLUMN_COINS + " INTEGER, "
                + GAME_COLUMN_SOUND + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + GAME_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertData() {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_MAXSTAGE, 0);
        contentValues.put(GAME_COLUMN_COINS, 0);
        contentValues.put(GAME_COLUMN_SOUND, 1);
        db.insert(GAME_TABLE_NAME, null, contentValues);
        return true;
    }


    public boolean insertStage(int stage) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_MAXSTAGE, stage);
        db.insert(GAME_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean updateStage(int stage, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_MAXSTAGE, stage);
        db.update(GAME_TABLE_NAME, contentValues, GAME_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + GAME_TABLE_NAME + " WHERE " +
                GAME_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }


    public boolean insertCoin(int coin){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_COINS, coin);
        db.insert(GAME_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean updateCoin(int coin, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_COINS, coin);
        db.update(GAME_TABLE_NAME, contentValues, GAME_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }



    public boolean insertSound(int sound){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_SOUND, sound);
        db.insert(GAME_TABLE_NAME, null,contentValues);
        return true;
    }

    public boolean updateSound(int sound, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAME_COLUMN_SOUND, sound);
        db.update(GAME_TABLE_NAME, contentValues,GAME_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }

}
