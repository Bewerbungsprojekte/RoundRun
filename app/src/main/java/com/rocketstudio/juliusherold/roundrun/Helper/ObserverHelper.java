package com.rocketstudio.juliusherold.roundrun.Helper;

import android.content.Context;

import com.rocketstudio.juliusherold.roundrun.Observer.DataObserver;

/**
 * Created by juliusherold on 08.04.17.
 */

public class ObserverHelper {
    DataObserver dataObserver;

    private static ObserverHelper _instance;


    public static ObserverHelper getInstance() {
        if (_instance == null) {
            _instance = new ObserverHelper();
        }
        return _instance;
    }

    public ObserverHelper(){
        dataObserver = new DataObserver();

    }

    public DataObserver getObserver(){
        return dataObserver;
    }
}
