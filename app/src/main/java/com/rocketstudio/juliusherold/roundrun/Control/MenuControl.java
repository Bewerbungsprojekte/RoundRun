package com.rocketstudio.juliusherold.roundrun.Control;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.games.Games;
import com.rocketstudio.juliusherold.roundrun.GameActivity;
import com.rocketstudio.juliusherold.roundrun.Helper.DBHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.SoundHelper;
import com.rocketstudio.juliusherold.roundrun.R;

/**
 * Created by herold on 06.04.2017.
 */

public class MenuControl extends LinearLayout {

    Context context;
    Cursor cursor;
    ImageView leaderboardIV,volumeIV;
    int sound = 1;

    public MenuControl(Context context, AttributeSet attr) {
        super(context, attr);
        this.context = context;
        initControl();
    }

    private void initControl() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_control, this);

        findViews();
        setImage();
        onClicks();

    }

    private void setImage() {
        DBHelper.getInstance(context).getData(1);

        cursor = DBHelper.getInstance(context).getData(1);
        cursor.moveToFirst();

        sound = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_SOUND));

        if (sound == 1){
            volumeIV.setImageResource(R.drawable.volume);
            SoundHelper.getInstance(context).playSound();
        } else if (sound == 0){
            volumeIV.setImageResource(R.drawable.mute);
        }

    }

    private void onClicks() {


        leaderboardIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        volumeIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sound == 1){
                    volumeIV.setImageResource(R.drawable.mute);
                    DBHelper.getInstance(context).updateSound(0,1);
                    SoundHelper.getInstance(context).stopSound();
                    sound = 0;
                }else {
                    volumeIV.setImageResource(R.drawable.volume);
                    DBHelper.getInstance(context).updateSound(1,1);
                    SoundHelper.getInstance(context).playSound();
                    sound = 1;
                }

            }
        });


    }

    private void findViews() {
        leaderboardIV = (ImageView) findViewById(R.id.leaderboardIV);
        volumeIV = (ImageView) findViewById(R.id.volumeIV);
    }

}
