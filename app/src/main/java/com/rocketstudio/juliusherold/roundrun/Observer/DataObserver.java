package com.rocketstudio.juliusherold.roundrun.Observer;

import java.util.Observable;

/**
 * Created by juliusherold on 08.04.17.
 */

public class DataObserver extends Observable {
    private int coins = 0;
    private int currentStage = 0;
    private int maxStage = 0;

    public int getCoins(){
        return coins;
    }

    public int getMaxStage(){ return maxStage;}

    public int getCurrentStage(){
        return currentStage;
    }



    public void setValue(int coins, int currentStage,int maxStage) {
        this.coins = coins;
        this.currentStage = currentStage;
        this.maxStage = maxStage;
        setChanged();
        notifyObservers();
    }
}
