package com.rocketstudio.juliusherold.roundrun.Helper;

import android.content.Context;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.games.Game;
import com.rocketstudio.juliusherold.roundrun.Dialog.FailDialog;
import com.rocketstudio.juliusherold.roundrun.GameActivity;

/**
 * Created by herold on 07.04.2017.
 */

public class GameHelper {
    Context context;
    private static GameHelper _instance;

    public static GameHelper getInstance(Context context){

        if (_instance == null){
            _instance = new GameHelper(context);
        }
        return _instance;

    }


    public GameHelper(Context context){
        this.context = context;
    }

    public void checkWin(TextView textView, ProgressBar progressBar){
        int goal = Integer.parseInt(textView.getText().toString());
        int current = progressBar.getProgress();

        if (current <= goal+10 && current >= goal-10){
            win();
        }else {
            fail();
        }
    }

    private void win() {

        VariableStorage.getInstance(context).updatePosition();

    }

    private void fail(){

    }

}
