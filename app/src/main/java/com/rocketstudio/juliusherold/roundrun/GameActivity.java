package com.rocketstudio.juliusherold.roundrun;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.rocketstudio.juliusherold.roundrun.Dialog.FailDialog;
import com.rocketstudio.juliusherold.roundrun.Helper.DBHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.GameHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.ProgressBarUpdater;
import com.rocketstudio.juliusherold.roundrun.Helper.SoundHelper;

import java.util.Random;

public class GameActivity extends BaseGameActivity{

    ProgressBar pbCircle;
    GoogleApiClient googleApiClient;
    ProgressBarUpdater progressbarupdater;
    RewardedVideoAd rewardedVideoAd;
    ImageView leaderboardIV,volumeIV;
    GameHelper gameHelper;
    Cursor cursor;
    int gameCounter = 0;
    int sound = 1;
    Context context;
    TextView counterTV;
    private Handler updateHandler = new Handler();


    private static GameActivity _instance;

    public static GameActivity getInstance() {

        if (_instance == null) {
            _instance = new GameActivity();
        }
        return _instance;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initialisieren();
        setContentView(R.layout.activity_game);

this.context = getApplicationContext();

        findViews();
        setImage();
        setFont();


        progressbarupdater = new ProgressBarUpdater(updateHandler, pbCircle, true, false);
        updateHandler.post(progressbarupdater);
        onClicks();
    }

    @Override
    protected void onPause() {
        SoundHelper.getInstance(getApplicationContext()).stopSound();
        super.onPause();
    }

    @Override
    protected void onStop() {
        SoundHelper.getInstance(getApplicationContext()).stopSound();
        super.onStop();
    }

    @Override
    protected void onResume() {

        cursor = DBHelper.getInstance(getApplicationContext()).getData(1);
        cursor.moveToFirst();

        int sound = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_SOUND));

        if (sound == 1){
            SoundHelper.getInstance(getApplicationContext()).playSound();
        }


        super.onResume();
    }

    private void initialisieren() {

        googleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API).addScope(Drive.SCOPE_FILE).build();
        try{
            googleApiClient.connect();
        }catch (Exception e){
            e.printStackTrace();
        }


        cursor = DBHelper.getInstance(getApplicationContext()).getData(1);
        cursor.moveToFirst();
        int sound = 1;

        try {
            sound = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_SOUND));

            if (sound ==1){
                SoundHelper.getInstance(getApplicationContext()).playSound();
            } else if (sound ==0){
                SoundHelper.getInstance(getApplicationContext()).playSound();
                SoundHelper.getInstance(getApplicationContext()).stopSound();
            }


        } catch (Exception e) {
            DBHelper.getInstance(getApplicationContext()).insertData();
        }

    }



    private void setFont() {

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/din.ttf");
        counterTV.setTypeface(face);
        counterTV.setText(random() + "");

    }



    private void setImage() {
        DBHelper.getInstance(context).getData(1);

        cursor = DBHelper.getInstance(context).getData(1);
        cursor.moveToFirst();

        sound = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_SOUND));

        if (sound == 1){
            volumeIV.setImageResource(R.drawable.volume);
            SoundHelper.getInstance(context).playSound();
        } else if (sound == 0){
            volumeIV.setImageResource(R.drawable.mute);
        }

    }

    public void createDialog(int position) {
        FailDialog failDialog = new FailDialog(GameActivity.this, position);
        failDialog.setCancelable(false);
        failDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                cursor = DBHelper.getInstance(getApplicationContext()).getData(1);
                cursor.moveToFirst();

                int maxStage = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_MAXSTAGE));


                gameCounter++;

                try {
                    Games.Leaderboards.submitScore(getApiClient(), getApplicationContext().getString(R.string.leaderboardid), maxStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        });
        failDialog.show();


    }

    private void onClicks() {
        pbCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int goal = Integer.parseInt(counterTV.getText().toString());

                if (pbCircle.getProgress() <= goal + 3 && pbCircle.getProgress() >= goal - 3) {
                    GameHelper.getInstance(getApplicationContext()).checkWin(counterTV, pbCircle);
                } else {
                    createDialog(pbCircle.getProgress());
                }


                counterTV.setText(random() + "");
            }
        });

        leaderboardIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(getApiClient(), getString(R.string.leaderboardid)), 1);
                }catch (Exception e){
                    e.printStackTrace();
                }
                }
        });

        volumeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sound == 1){
                    volumeIV.setImageResource(R.drawable.mute);
                    DBHelper.getInstance(context).updateSound(0,1);
                    SoundHelper.getInstance(context).stopSound();
                    sound = 0;
                }else {
                    volumeIV.setImageResource(R.drawable.volume);
                    DBHelper.getInstance(context).updateSound(1,1);
                    SoundHelper.getInstance(context).playSound();
                    sound = 1;
                }

            }
        });
    }

    private void findViews() {

        pbCircle = (ProgressBar) findViewById(R.id.progressBar);
        counterTV = (TextView) findViewById(R.id.value);
        leaderboardIV = (ImageView) findViewById(R.id.leaderboardIV);
        volumeIV = (ImageView) findViewById(R.id.volumeIV);

    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    public int random() {

        Random r = new Random();
        int low = 1;
        int heigh = 100;
        int result = r.nextInt(heigh - low) + low;
        return result;
    }

}


