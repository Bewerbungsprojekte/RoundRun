package com.rocketstudio.juliusherold.roundrun.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.rocketstudio.juliusherold.roundrun.Helper.GameHelper;
import com.rocketstudio.juliusherold.roundrun.Helper.VariableStorage;
import com.rocketstudio.juliusherold.roundrun.R;

/**
 * Created by juliusherold on 09.04.17.
 */

public class FailDialog extends Dialog {

    Button addButton, restartButton;
    TextView progressTextView, headerTextView, infoTextView;

    int position;
    Context context;

    public FailDialog(Context context, int position) {
        super(context);
        this.position = position;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fail_dialog);
        initControl();
    }

    private void initControl() {


        findViews();
        setFont();
        progressTextView.setText(position+"");
        onClicks();
    }



    private void onClicks() {
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VariableStorage.getInstance(getContext()).resetPosition();
                dismiss();
            }
        });

    }

    private void findViews() {
        progressTextView = (TextView) findViewById(R.id.progressTV);
        restartButton = (Button) findViewById(R.id.resetBTN);
        headerTextView = (TextView) findViewById(R.id.headerTV);
        infoTextView = (TextView) findViewById(R.id.infoTV);
    }

    private void setFont() {

        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/din.ttf");
        headerTextView.setTypeface(face);
        infoTextView.setTypeface(face);
        progressTextView.setTypeface(face);

    }


}
