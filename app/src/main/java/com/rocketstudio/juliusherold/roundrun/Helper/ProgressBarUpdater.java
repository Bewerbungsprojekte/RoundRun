package com.rocketstudio.juliusherold.roundrun.Helper;

import android.os.Handler;
import android.widget.ProgressBar;

/**
 * Created by juliusherold on 05.04.17.
 */

public class ProgressBarUpdater implements Runnable {

    int progress;
    Handler updateHandler;
    boolean up = true;
    boolean stop = false;
    ProgressBar progressBar;


    public ProgressBarUpdater(Handler updateHandler, ProgressBar progressBar, boolean up, boolean stop){
        this.updateHandler = updateHandler;
        this.progressBar = progressBar;
        this.up = up;
        this.stop = stop;
    }

    @Override
    public void run() {

        if (stop){
            updateHandler.removeCallbacks(this);
            updateHandler = null;
        }else {
            setValue();
        }

        updateHandler.postDelayed(new ProgressBarUpdater(updateHandler, progressBar,up,stop),9);
    }

    public void stop(){
        stop = true;
    }



    private void setValue() {

        if (up == true){
            increment(progressBar.getProgress());
        }else {
            decrement(progressBar.getProgress());
        }

    }

    private void decrement(int progress) {
        progress--;
        progressBar.setProgress(progress);
        if (progress == 0){
            up = true;
        }

    }

    private void increment(int aktuell) {
        aktuell++;
        progressBar.setProgress(aktuell);
        if (aktuell == 100){
            up = false;
        }
    }
}
