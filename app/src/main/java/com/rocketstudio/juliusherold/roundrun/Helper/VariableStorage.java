package com.rocketstudio.juliusherold.roundrun.Helper;

import android.content.Context;
import android.database.Cursor;

import com.google.android.gms.games.Games;
import com.rocketstudio.juliusherold.roundrun.Control.GameDataControl;
import com.rocketstudio.juliusherold.roundrun.GameActivity;
import com.rocketstudio.juliusherold.roundrun.Observer.DataObserver;
import com.rocketstudio.juliusherold.roundrun.R;

/**
 * Created by herold on 07.04.2017.
 */

public class VariableStorage {

    private int currentPosition = 0;
    private int coinCounter = 0;
    private int maxStage = 0;
    private int currentCoins;
    private static VariableStorage _instance;
    private Context context;
    private Cursor cursor;



    public static VariableStorage getInstance(Context context) {
        if (_instance == null) {
            _instance = new VariableStorage(context);
        }
        return _instance;
    }

    public VariableStorage(Context context) {
        this.context = context;

            cursor = DBHelper.getInstance(context).getData(1);
            cursor.moveToFirst();
            currentCoins = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_COINS));
            maxStage = cursor.getInt(cursor.getColumnIndex(DBHelper.GAME_COLUMN_MAXSTAGE));
    }

    public void updatePosition() {
        currentPosition++;
        updateCoinCounter();
        if (maxStage < currentPosition){
            updateMaxPosition();
        }
        updateDataControl();

    }

    private void updateMaxPosition() {
        maxStage = currentPosition;
        DBHelper.getInstance(context).updateStage(currentPosition,1);
    }

    public void updateCoinCounter() {
        coinCounter++;
        if (coinCounter == 5) {
            coinCounter = 0;
            updateCurrentCoins();
        }
    }

    public void updateCurrentCoins() {
        currentCoins++;
        DBHelper.getInstance(context).updateCoin(currentCoins, 1);
    }

    private void updateDataControl(){
        ObserverHelper.getInstance().getObserver().setValue(currentCoins,currentPosition,maxStage);
    }

    public void resetPosition(){
        currentPosition = 0;
        ObserverHelper.getInstance().getObserver().setValue(currentCoins,currentPosition,maxStage);
    }



}
